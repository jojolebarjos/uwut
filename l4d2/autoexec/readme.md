
# Ü-WUT autoexec config file 1.0.2


## Install

Just copy/replace this file in `<steam>\SteamApps\common\Left 4 Dead 2\left4dead2\cfg`.


## Features

Based on Sir Please's configuration file.

 - Change tickrate to 100, if server supports this feature
 - Show network graph and statistics when score board is shown
 - List IPs of some dedicated servers (U-WUT, pWg, Hyper-V)

