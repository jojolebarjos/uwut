﻿
# Ü-WUT Pro Mod 1.1.0


## Install

Copy files in L4D2 install folder.

Requires:

 - ProMod 4.2.2 (http://www.l4dnation.com/community-news/pro-mod-4-2-2/)
 - Pounce Damage Uncap 1.1 (https://github.com/ProdigySim/Pounce-Damage-Uncap/releases)
 - Tickrate Enabler 1.4 (https://github.com/Satanic-Spirit/Tickrate-Enabler)

If you do not want to use our `matchmode.txt` (i.e. you already have some other custom mods), manually add in 4v4 category:

```
		"uwut"
		{
			"name" "ProMod U-WUT"
		}
```

Unzip `tickrate_enabler_1.4.zip` files in the addons folder.
Correct setup is : 

```
../addons/tickrate_enabler.vdf
../addons/tickrate_enabler/tickrate_enabler.so 
```

To enable the tickrate enabler add this command in the server launch parameters:

```
-tickrate 100
```

Select the tickrate in ```cfg\confogl_rates.cfg``` by un/commenting according to the desired configuration. (note: this file seems 
to override the parameters from serfer.cfg).


## Features

Based on Pro Mod 4.2.2, with the following changes:

 - Round starts when everyone is ready, regardless of actual player count
 - Remove early tank (min 35%)
 - Smoker tongue is faster (2x), has higher range (2x) and survivor accelerates faster (3x)
 - Smoker can now choose to break his own tongue (and flee)
 - Uncap and increase hunter pounce damage (+50%)
 - Hunter jump can destroy doors again by pouncing (like in vanilla)
 - Allow one nerfed hunting rifle per team (only 10 bullets in clip, damage against tank reduced, fire rate halved)
 - Add 1 first aid kit, 1 pipebomb, 1 hunting rifle and 1 machete in starting saferoom
 - Disable BotPopStop plugin (forbid bots to use pills, or so they say), which caused bots to waste pills without any effect
 - Allow infected bots to spawn if less than 4 humans infected players


## Bugs and todo list

 - Some medkits still spawn (for instance first map of Death Toll)
 - Bots are stuck trying to get a hunting rifle, which is forbidden if someone already has one
 - To solve both issues, remove medpack and hunting rifles at round start
 - Spawned items can fall/roll out of reach due to physics (i.e. need to spawn them as static?)
 - Allow non-conventional special infected compositions (for instance 2 smokers)?
 - Jockey pounce infos?
 - Increase melee weapons on map?
 - Remove flow tank on final? (see final_tank_blocker.sp and witch_and_tankifier.sp)
 - Bots pick up items and give them to humans? Humans can give medpack and throwable?
 - !concombre
 - !flow, etc.
 - View distance for hittable aura, for tank
 - Find a way to directly start with ProMod


## Links and ideas

 - AlliedModders https://forums.alliedmods.net/index.php
 - Valve developer wiki https://developer.valvesoftware.com/wiki/Weapon_smg_spawn
 - Pawn compiler http://www.sourcemod.net/compiler.php
 - Convar list https://developer.valvesoftware.com/wiki/List_of_L4D2_Cvars
 - L4DNation forum http://www.l4dnation.com/confogl-and-other-configs/
 - ProMod repository https://github.com/jacob404/Pro-Mod-4.0
 - ProMod plugins https://github.com/Stabbath/ProMod/tree/master/addons/sourcemod/scripting
 - Misc SourceMod plugins https://bitbucket.org/ProdigySim/misc-sourcemod-plugins
 - ReadyUp source code https://github.com/MatthewClair/l4d2readyup/blob/master/readyup.sp
 - Smoke it https://forums.alliedmods.net/showthread.php?t=103994
 - Smoke cloud https://forums.alliedmods.net/showthread.php?p=866613
 - Give items https://forums.alliedmods.net/showthread.php?p=1294082
 - Crawl https://forums.alliedmods.net/showthread.php?p=1291588
 - Smoker imba https://forums.alliedmods.net/showthread.php?t=234442
 - Jockey ride incap https://forums.alliedmods.net/showthread.php?t=216739
 - Allow some more "random" set of infected (multiple smoker, etc)?
 - Remove weapon restriction for T1?
 - Allow give throwable? https://forums.alliedmods.net/showthread.php?p=1128204
 - Witch control? of course, without automatic attack https://forums.alliedmods.net/showthread.php?t=125591
 - Charger steering! https://forums.alliedmods.net/showthread.php?t=179034
 - Roar? https://forums.alliedmods.net/showthread.php?t=126919
 - Use some code from special infected selection manager? https://forums.alliedmods.net/showthread.php?t=121461
 - Special common? https://forums.alliedmods.net/showthread.php?t=175242
 - Moving smoker? https://forums.alliedmods.net/showthread.php?t=103994
 - Eat shit to get up? https://forums.alliedmods.net/showthread.php?t=109655
 - Pipe in da mouth? https://forums.alliedmods.net/showthread.php?t=188066
 - Throw grenade while incapped? https://forums.alliedmods.net/showthread.php?t=122224
 - Custom grenade launcher https://forums.alliedmods.net/showthread.php?t=175241?t=175241
 - Deal damage https://forums.alliedmods.net/showthread.php?t=111684?t=111684


## History and changelog

 - 07.02.15: 1.0.0 released
 - 27.12.14: development started
